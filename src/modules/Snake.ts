class Snake {
    // 蛇头元素
    head: HTMLElement;
    // 蛇的身体（包括蛇头）
    bodies: HTMLCollection;
    // 蛇的容器
    element: HTMLElement

    constructor() {
        this.element = document.getElementById('snake');
        this.head = document.querySelector('#snake > div');
        this.bodies = this.element.getElementsByTagName('div');
    }

    // 获得蛇头 X 轴坐标
    get X():number {
        return this.head.offsetLeft;
    }
    
    // 获得蛇头 Y 轴坐标
    get Y():number {
        return this.head.offsetTop;
    }

    // 设置蛇头 X 轴坐标
    set X(value) {
        if (this.X === value) return;

        // X的值的合法范围在0-290之间
        if (value < 0 || value > 290) {
            throw new Error("蛇撞墙了！"); 
        }

        // 防止长度大于一时原地掉头现象
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value) {
            if (value > this.X) {
                value = this.X - 10;
            } else {
                value = this.X + 10;
            }
        }

        // 移动身体
        this.moveBody();
        this.head.style.left = value +'px';
        // 检查有没有撞到自己
        this.checkHeadBody()
    }

     // 设置蛇头 Y 轴坐标
     set Y(value) {
        if (this.Y === value) return;

        // X的值的合法范围在0-290之间
        if (value < 0 || value > 290) {
            throw new Error("蛇撞墙了！"); 
        }

        // 防止长度大于一时原地掉头现象
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value) {
            if (value > this.Y) {
                value = this.Y - 10;
            } else {
                value = this.Y + 10;
            }
        }

        // 移动身体
        this.moveBody();
        this.head.style.top = value +'px';
        // 检查有没有撞到自己
        this.checkHeadBody()
    }

    // 蛇增加身体长度的方法
    addBody() {
        // 向element中添加一个div
        this.element.insertAdjacentHTML("beforeend","<div></div>");
    }

    // 蛇移动身体的方法
    moveBody() {
        // 后边身体的位置设置为前边身体的位置
        for (let i = this.bodies.length - 1; i > 0; i--) {
            // 获取前边身体的位置
            let X = (this.bodies[i-1] as HTMLElement).offsetLeft;
            let Y = (this.bodies[i-1]as HTMLElement).offsetTop;

            // 更新自己的位置
            (this.bodies[i] as HTMLElement).style.left = X + 'px';
            (this.bodies[i] as HTMLElement).style.top = Y + 'px';
        }
    }

    // 检查有没有撞到自己的方法
    checkHeadBody() {
        // 检查蛇头和身体节点的坐标是否发生重叠
        for (let i = 1; i < this.bodies.length; i++) {
            let bd = this.bodies[i] as HTMLElement;
            if (this.X === bd.offsetLeft && this.Y === bd.offsetTop) {
                // 发生了重叠代表撞到了自己，游戏结束
                throw new Error("撞到了自己！");
                
            }
        }
    }
}
export default Snake;