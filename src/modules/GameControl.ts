import Snake from './Snake';
import Food from './Food';
import ScorePanel from './ScorePanel';

// 游戏控制器
class GameControl {
    snake: Snake;
    food: Food;
    scorePanel: ScorePanel;
    // 存储蛇的移动方向（也就是按键方向）
    direction: string;
    // 游戏状态（记录游戏是否结束）
    isLive: boolean;

    constructor() {
        this.snake = new Snake();
        this.food = new Food();
        this.scorePanel = new ScorePanel(10, 3);
        this.direction = '';
        this.isLive = true;

        this.init();
    }

    // 游戏的初始化方法，调用后游戏即开始
    init() {
        // 绑定键盘事件
        document.addEventListener('keydown',this.keydownHandler.bind(this));
        this.run();
    }

    keydownHandler(event: KeyboardEvent) {
        // TODO: 检查 event.key 是否合法（用户是否按了正确的键）
        let name: string[] = ["ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight", " "];
        if (name.includes(event.key)) {
            // 修改 direction 属性
            this.direction = event.key;
        }
    }

    // 控制蛇移动的方法
    run() {
        // 获取蛇头现在的位置
        let X = this.snake.X;
        let Y = this.snake.Y;

        switch (this.direction) {
            // 上键
            case "ArrowUp":
                Y -= 10;
                break;
            // 下键
            case "ArrowDown":
                Y += 10;
                break;
            // 左键
            case "ArrowLeft":
                X -= 10;
                break;
            // 右键
            case "ArrowRight":
                X += 10;
                break;
            default:
                break;
        }

        // 检查蛇是否吃到了食物
        this.checkEat(X, Y);

        // 修改蛇头的位置
        try {
            this.snake.X = X;
            this.snake.Y = Y;
        } catch (error) {
            alert(error.message + ' GAME OVER!');
            // 游戏结束
            this.isLive = false;
        }

        // 开启一个定时调用，并且随着level提高缩短时间（等于随level提升了蛇的移动速度）
        this.isLive && setTimeout(this.run.bind(this), 300 - (this.scorePanel.level - 1) * 30);
    }

    // 定义一个方法，用来检查蛇是否吃到食物
    checkEat(X: number, Y: number) {
        if (X === this.food.X && Y === this.food.Y) {
            // 食物位置重置
            this.food.change();
            // 分数提高
            this.scorePanel.addScore();
            // 蛇身增加一节
            this.snake.addBody();
        }
    }
}

export default GameControl;