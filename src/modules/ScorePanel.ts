// 定义表示积分牌的类
class ScorePanel {
    score: number;// 分数
    level: number;// 等级
    
    // 分数和等级所在的元素
    scoreElement: HTMLElement;
    levleElement: HTMLElement;

    // 设计一个变量限制等级
    maxLevel: number;
    // 设计一个变量表示多少分数时升一级
    upScore: number;

    constructor(maxLevel: number = 10, upScore: number = 10) {
        this.scoreElement = document.getElementById('score');
        this.levleElement = document.getElementById('level');
        this.score = 0;
        this.level = 1;
        this.maxLevel = maxLevel;
        this.upScore = upScore;
    }

    // 加分的方法
    addScore() {
        this.scoreElement.innerText = ++this.score + '';
        if (this.score % this.upScore === 0) {
            this.levelUp();
        }
    }

    //升级的方法
    levelUp() {
        if (this.level < this.maxLevel) {
            this.levleElement.innerText = ++this.level + '';
        }
    }
}

export default ScorePanel;