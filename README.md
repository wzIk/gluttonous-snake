# Gluttonous Snake

#### 介绍

本人小白一个，在学习ts的过程中为了巩固所学的知识，写了这样的一个贪吃蛇小游戏，后续可能会更新修改一些细节上的问题。

#### 安装教程

基于node.js, 所以先确定你是否安装了node.js
1.  安装依赖 npm -i
2.  运行项目 npm run start
3.  打包项目 npm run build

#### 使用说明

运行项目后会自动打开浏览器窗口，按照游戏说明去玩游戏吧！

 ##### 更新
 删除了dist文件夹，如有需要，运行 npm run build 进行打包

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
