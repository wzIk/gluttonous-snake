const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');

// webpack配置
module.exports = {
    // 指定入口文件
    entry: "./src/index.ts",
    // 指定打包文件输出目录
    output: {

        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        // 不能使用箭头函数
        environment: {
            arrowFunction: false
        }
    },
    // 指定webpack打包时使用的模块
    module: {
        // 指定要加载的规则
        rules: [{
                // 指定规则生成文件
                test: /\.ts$/,
                // 使用的loader
                use: [{
                        // 指定加载器
                        loader: 'babel-loader',
                        options: {

                            // 设置预定义环境
                            presets: [
                                [
                                    // 指定环境的插件
                                    "@babel/preset-env",
                                    // 配置信息
                                    {
                                        // 要兼容的浏览器
                                        targets: {
                                            "chrome": "88",
                                            // "ie": "11"
                                        },
                                        "corejs": "3",
                                        // 按需加载
                                        "useBuiltIns": "usage"
                                    }
                                ]
                            ]
                        }
                    },
                    'ts-loader'
                ],
                // 要排除的文件
                exclude: /node_modules/,
            },
            // 设置less文件的处理
            {
                test: /\.less$/,
                use: [
                    // 从下往上执行，顺序固定
                    "style-loader",
                    "css-loader",
                    // 引入postcss
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "postcss-preset-env", {
                                            browser: "last 2 versions"
                                        }
                                    ]
                                ]
                            }
                        }

                    },
                    "less-loader"
                ]

            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: "VSCODE",
            template: "./src/index.html"
        }),
    ],
    mode: 'none',
    // 用来设置可以引用的模块
    resolve: {
        extensions: ['.ts', '.js']
    }
}