# Gluttonous Snake

#### Description

I am a little white, in the process of learning TS in order to consolidate the knowledge learned, wrote such a snake game, the follow-up may update some details of the problem. 

#### Installation

Based on Node.js, so make sure you have Node.js installed  
1. The installation depends on NPM -I  
2. Run the project NPM run start  
3. Package the project NPM run build  

#### Instructions

Run the project will automatically open the browser window, according to the game instructions to play the game!  
 ##### update
 Remove the dist folder and run NPM Run Build to package it if necessary  

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
